#SINEVIA AUTHENTICATION

Authentication package

# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/authentication.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/authentication": "dev-master"
    },
```

# How to Use? #